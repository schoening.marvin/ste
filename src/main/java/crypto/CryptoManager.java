package crypto;

import crypto.pbe.PasswordBasedKey;
import crypto.signature.DigitalSignature;
import crypto.symmetric.AesDes;
import crypto.symmetric.Gcm;
import crypto.verification.Hash;
import crypto.verification.Macs;

import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Hex;

/**
 * Handles input from Editor App.
 * Defines which methods should be called from given input
 *
 * @author Marvin Schöning
 */
public class CryptoManager {

  private final int keySize;
  private final String input;
  private final String encryptionType;
  private final String blockmode;
  private final String padding;
  private final String digestName;
  private final String hashOrMac;
  private final String signatureString;
  private final String password;
  private final String salt;
  private final String passwordEncryptionType;
  private final String[] keyString;

  private static int COST_PARAMETER = 32768;
  private static int BLOCKSIZE = 8;
  private static int PARALLELIZATION_PARAM = 1;


  /**
   * Constructor for encryption.
   *
   * @param input          text to encrypt
   * @param encryptionType e.g. "AES"
   * @param blockmode      e.g. CBC or ECB
   * @param padding        e.g. PKCS7Padding
   * @param digestName     e.g. SHA-256
   * @param keySize        e.g. 256
   * @param password       user password
   */
  public CryptoManager(
          String input, String encryptionType,
          String blockmode, String padding,
          String digestName, int keySize,
          String password, String passwordEncryptionType) {
    this.encryptionType = encryptionType;
    this.blockmode = blockmode;
    this.padding = padding;
    this.input = input;
    this.keySize = keySize;
    this.keyString = null;
    this.digestName = digestName;
    this.hashOrMac = null;
    this.signatureString = null;
    this.password = password;
    this.salt = null;
    this.passwordEncryptionType = passwordEncryptionType;
  }

  /**
   * Constructor for decryption.
   *
   * @param input           encrytped text
   * @param encryptionType  encryptionType e.g. "AES"
   * @param blockmode       CBC/ECB/OBF etc
   * @param padding         NoPadding/PKCS7Padding/ZeroBytePadding
   * @param digestName      hash or mac algorithm name
   * @param hashOrMac       hash or mac value from algorithm
   * @param signatureString signature string
   * @param keyArray        [0] encryption key, [1] iv, [2] macKey, [3] signature public key
   * @param password        password that was used, ""/null if no password was used
   * @param salt            salt that was used for the password
   */
  public CryptoManager(
          String input,
          String encryptionType,
          String blockmode,
          String padding,
          String digestName,
          String hashOrMac,
          String signatureString,
          String[] keyArray,
          String password,
          String salt,
          String passwordEncryptionType
  ) {
    this.encryptionType = encryptionType;
    this.blockmode = blockmode;
    this.padding = padding;
    this.input = input;
    this.keySize = 0;
    this.keyString = Arrays.copyOf(keyArray, 7);
    this.digestName = digestName;
    this.hashOrMac = hashOrMac;
    this.signatureString = signatureString;
    this.password = password;
    this.salt = salt;
    this.passwordEncryptionType = passwordEncryptionType;
  }

  /**
   * Encrypt with class parameters.
   *
   * @return [0] key,
   * [1] encrypted message,
   * [2] iv,
   * [3] hashOrMac,
   * [4] macKey,
   * [5] signature,
   * [6] signature public key,
   * [7] salt
   */
  public String[] encrypt() throws Exception {
    int arrayLength = 8;
    String[] returnArray = new String[arrayLength];

    // Generate Key depending on password
    SecretKey key;
    if (password.equals("")) {
      key = Utility.generateKey(encryptionType, keySize);
      returnArray[0] = Hex.toHexString(key.getEncoded());
    } else {
      if (passwordEncryptionType.equals("SCrypt")) {
        byte[] saltArray = Hex.encode(Utility.getNextSalt());
        byte[] scryptKeyArray = PasswordBasedKey.jceSCrypt(
                password.toCharArray(),
                saltArray,
                COST_PARAMETER,
                BLOCKSIZE,
                PARALLELIZATION_PARAM
        );
        key = new SecretKeySpec(scryptKeyArray, 0, scryptKeyArray.length, encryptionType);
        returnArray[7] = Hex.toHexString(saltArray); // Salt
      } else {
        String[] pbeArray = PasswordBasedKey.generate(passwordEncryptionType, password);
        byte[] keyBytes = Hex.decode(pbeArray[0]);
        key = new SecretKeySpec(keyBytes, 0, keyBytes.length, passwordEncryptionType);
        returnArray[7] = pbeArray[1];
      }
    }

    //Encrypt
    String[] encryptArray;
    if (blockmode.equals("GCM") || blockmode.equals("CCM")) {
      encryptArray = Gcm.gcmEncrypt(key, input.getBytes(StandardCharsets.UTF_8));
      returnArray[1] = encryptArray[0];
      returnArray[2] = encryptArray[1];
    } else {
      encryptArray =
              AesDes.encrypt(input, encryptionType, key, blockmode, padding);
      returnArray[1] = encryptArray[0];
      returnArray[2] = encryptArray[1];

    }


    // Save hash
    if (digestName.equals("SHA-256")) {
      byte[] hash = Hash.computeDigest(digestName, Hex.decode(returnArray[1]));
      returnArray[3] = Hex.toHexString(hash);
    } else {
      SecretKey macKey;
      if (digestName.equals("AESCMAC")) {
        macKey = Utility.generateKey("AES");
      } else {
        macKey = Utility.generateKey("HmacSHA256");
      }

      byte[] mac = Macs.computeMac(digestName, macKey, Hex.decode(returnArray[1]));
      returnArray[3] = Hex.toHexString(mac);
      returnArray[4] = Hex.toHexString(macKey.getEncoded());
    }

    // Apply signature
    KeyPair kp = DigitalSignature.generateDsaKeyPair(DigitalSignature.generateDsaParams(2048));
    byte[] signature = DigitalSignature.generateDsaSignature(
            kp.getPrivate(),
            Hex.decode(returnArray[1])
    );
    returnArray[5] = Hex.toHexString(signature);
    returnArray[6] = Hex.toHexString(kp.getPublic().getEncoded());

    return returnArray;
  }

  /**
   * Decrypt with class parameters.
   *
   * @return decrypted message as string
   * @throws Exception Exceptions
   */
  public String decrypt() throws Exception {

    byte[] byteInput = Hex.decode(input);

    // Check digital signature
    PublicKey publicKey = KeyFactory.getInstance("DSA").generatePublic(
            new X509EncodedKeySpec(Hex.decode(keyString[3]))
    );

    DigitalSignature.verifyDsaSignature(publicKey, byteInput, Hex.decode(signatureString));

    // Check Hash or Mac
    if (digestName.equals("SHA-256")) {
      byte[] hash = Hash.computeDigest(digestName, byteInput);
      if (!Hex.toHexString(hash).equals(hashOrMac)) {
        throw new Exception("Hash not equal");
      }
    } else {
      SecretKey macKey;
      if (digestName.equals("HmacSHA256")) {
        macKey = Utility.loadKey(keyString[2], digestName);
      } else {
        macKey = Utility.loadKey(keyString[2], encryptionType);
      }
      byte[] mac = Macs.computeMac(digestName, macKey, byteInput);
      if (!Hex.toHexString(mac).equals(hashOrMac)) {
        throw new Exception("Mac not equal");
      }
    }

    //Generate Key depending on password

    SecretKey key;
    if (password.equals("")) {
      key = Utility.loadKey(keyString[0], encryptionType);
    } else {
      if (passwordEncryptionType.equals("SCrypt")) {
        byte[] scryptKeyArray = PasswordBasedKey.jceSCrypt(
                password.toCharArray(),
                Hex.decode(salt),
                COST_PARAMETER,
                BLOCKSIZE,
                PARALLELIZATION_PARAM
        );

        key = new SecretKeySpec(scryptKeyArray, 0, scryptKeyArray.length, "AES");
      } else {
        key = PasswordBasedKey.load(passwordEncryptionType, password, Hex.decode(salt));
      }
    }

    // Generate IV if used
    byte[] iv = null;
    if (!keyString[1].equals("")) {
      iv = Utility.loadIV(keyString[1]);
    }

    // Decrypt
    if (blockmode.equals("GCM") || blockmode.equals("CCM")) {
      return new String(Gcm.gcmDecrypt(key, iv, byteInput), StandardCharsets.UTF_8);
    }

    return AesDes.decrypt(
            byteInput,
            encryptionType,
            blockmode,
            padding,
            key,
            iv);
  }
}


