package crypto.symmetric;

import crypto.Utility;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import org.bouncycastle.util.encoders.Hex;


public class AesDes {
  /**
   * Method to return a symmetric decrypted message.
   *
   * @param input          encrypted text
   * @param encryptionType type that was used for encryption
   * @param blockmode      blockmode that was used for encryption
   * @param padding        padding that was used for encryption
   * @param key            secretKey that was used for encryption
   * @param iv             iv that was used for encryption, null if none was used
   * @return decrypted message
   */
  public static String decrypt(
          byte[] input,
          String encryptionType,
          String blockmode,
          String padding,
          SecretKey key,
          byte[] iv
  )
          throws Exception {

    // Generate Cipher, load key and change input to hex
    Cipher cipher = Utility.generateCipher(encryptionType, blockmode, padding);

    // Init Cipher
    if (Utility.blockmodeWithoutIV.contains(blockmode)) {
      cipher.init(Cipher.DECRYPT_MODE, key);
    } else {
      cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
    }

    // set cipherText length
    byte[] cipherText = new byte[cipher.getOutputSize(input.length)];

    // Iterate through the full input
    int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
    ctLength += cipher.doFinal(cipherText, ctLength);

    //Return decrypted string
    return new String(Arrays.copyOfRange(cipherText, 0, ctLength), StandardCharsets.UTF_8);
  }

  /**
   * Method to return a symmetric encrypted message, the used key and iv.
   *
   * @param input          plainText for encryption
   * @param encryptionType e.g. AES
   * @param blockmode      e.g. ECB
   * @param padding        e.g. PKCS5Padding
   * @return [0] = encrypted and encoded message, [1] = encoded iv
   * @throws Exception Throws Exceptions
   */
  public static String[] encrypt(
          String input,
          String encryptionType,
          SecretKey key,
          String blockmode,
          String padding)
          throws Exception {

    // Return variable
    String[] encryptedArray = new String[2];

    // Init Cipher
    Cipher cipher = Utility.generateCipher(encryptionType, blockmode, padding);
    cipher.init(Cipher.ENCRYPT_MODE, key);
    if (!Utility.blockmodeWithoutIV.contains(blockmode)) {
      // create iv and save it
      byte[] iv = cipher.getIV();
      encryptedArray[1] = new String(Hex.encode(iv), StandardCharsets.UTF_8);
    }

    // Convert input into a byte[] byteInput for encryption
    byte[] byteInput = input.getBytes(StandardCharsets.UTF_8);

    // set cipherText length
    byte[] cipherText = new byte[cipher.getOutputSize(byteInput.length)];

    // Iterate through the full text
    int ctLength = cipher.update(byteInput, 0, byteInput.length, cipherText, 0);
    cipher.doFinal(cipherText, ctLength);

    // Save cipherText as hexString into return array
    encryptedArray[0] = Hex.toHexString(cipherText);

    return encryptedArray;
  }
}
