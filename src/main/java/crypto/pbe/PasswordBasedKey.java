package crypto.pbe;

import crypto.Utility;
import java.security.GeneralSecurityException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.generators.SCrypt;
import org.bouncycastle.util.encoders.Hex;



/**
 * Generates and load keys based on a password.
 *
 * @author Marvin Schöning
 */
public class PasswordBasedKey {

  /**
   * Generate a Password Based Key.
   *
   * @param encryptionType password encryption type e.g. PBEWithSHA256And128BitAES
   * @param password users input password
   * @return key based on users password and choosen encryption type
   */
  public static String[] generate(
          String encryptionType,
          String password)
          throws Exception {
    String[] encryptedArray = new String[2];

    byte[] salt = Hex.encode(Utility.getNextSalt());
    encryptedArray[1] = Hex.toHexString(salt); // Salt

    PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray(), salt, 100);

    SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(encryptionType);
    SecretKey secretKey = secretKeyFactory.generateSecret(pbeKeySpec);
    encryptedArray[0] = Hex.toHexString(secretKey.getEncoded());

    return encryptedArray;
  }

  /**
   * Loads a key based on user input password, given salt and encryption type.
   *
   * @param encryptionType type that was used for generating
   * @param password password used for generation
   * @param salt salt used for generation
   * @return password based key if input is correct
   */
  public static SecretKey load(
          String encryptionType,
          String password,
          byte[] salt)
          throws Exception {
    PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray(), salt, 100);

    SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(encryptionType);
    SecretKey secretKey = secretKeyFactory.generateSecret(pbeKeySpec);

    return secretKey;
  }

  public static byte[] jceSCrypt(char[] password, byte[] salt,
                                 int costParameter, int blocksize,
                                 int parallelizationParam) throws GeneralSecurityException {
    return SCrypt.generate(PBEParametersGenerator.PKCS5PasswordToUTF8Bytes(password),
            salt, costParameter, blocksize, parallelizationParam, 256 / 8);
  }
}
