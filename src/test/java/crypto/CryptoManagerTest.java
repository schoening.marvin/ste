package crypto;

import crypto.pbe.PasswordBasedKey;
import crypto.signature.DigitalSignature;
import crypto.symmetric.AesDes;
import crypto.symmetric.Gcm;
import crypto.verification.Hash;
import crypto.verification.Macs;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CryptoManagerTest {
  AesDes coverageAesDes = new AesDes();
  Gcm coverageGcm = new Gcm();
  DigitalSignature coverageDigitalSignature = new DigitalSignature();
  Hash hashCoverage = new Hash();
  Macs macCoverage = new Macs();
  PasswordBasedKey pbkCoverage = new PasswordBasedKey();

  @Test
  void encryptAesCbcWithoutPassword() {

    CryptoManager cm = new CryptoManager(
            "000102030405060708090a0b0c0d0e0f",
            "AES",
            "CBC",
            "PKCS5Padding",
            "SHA-256",
            256,
            "",
            "");
    try {
      String[] encrypt = cm.encrypt();
      // key
      assertNotEquals(null, encrypt[0]);
      // encrypted message
      assertNotEquals(null, encrypt[1]);
      // iv
      assertNotEquals(null, encrypt[2]);
      // hash
      assertNotEquals(null, encrypt[3]);
      // macKey
      assertEquals(null, encrypt[4]);
      // signature
      assertNotEquals(null, encrypt[5]);
      // signaturekey
      assertNotEquals(null, encrypt[6]);
      // salt
      assertEquals(null, encrypt[7]);
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }

  }

  @Test
  void encryptAesCbcWithPasswordSCrypt() {

    CryptoManager cm = new CryptoManager(
            "000102030405060708090a0b0c0d0e0f",
            "AES",
            "CBC",
            "PKCS5Padding",
            "SHA-256",
            256,
            "test123",
            "SCrypt");
    try {
      String[] encrypt = cm.encrypt();
      // key
      assertEquals(null, encrypt[0]);
      // encrypted message
      assertNotEquals(null, encrypt[1]);
      // iv
      assertNotEquals(null, encrypt[2]);
      // hash
      assertNotEquals(null, encrypt[3]);
      // macKey
      assertEquals(null, encrypt[4]);
      // signature
      assertNotEquals(null, encrypt[5]);
      // signaturekey
      assertNotEquals(null, encrypt[6]);
      // salt
      assertNotEquals(null, encrypt[7]);
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }

  }

  @Test
  void encryptAesCbcWithPasswordPBE() {
    CryptoManager cm = new CryptoManager(
            "000102030405060708090a0b0c0d0e0f",
            "AES",
            "CBC",
            "PKCS5Padding",
            "SHA-256",
            256,
            "test123",
            "PBEWithSHA256And128BitAES-CBC-BC");
    try {
      String[] encrypt = cm.encrypt();
      // key
      assertEquals(null, encrypt[0]);
      // encrypted message
      assertNotEquals(null, encrypt[1]);
      // iv
      assertNotEquals(null, encrypt[2]);
      // hash
      assertNotEquals(null, encrypt[3]);
      // macKey
      assertEquals(null, encrypt[4]);
      // signature
      assertNotEquals(null, encrypt[5]);
      // signaturekey
      assertNotEquals(null, encrypt[6]);
      // salt
      assertNotEquals(null, encrypt[7]);
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }

  }

  @Test
  void encryptAesCbcWithoutPasswordWithAESCMAC() {
    CryptoManager cm = new CryptoManager(
            "000102030405060708090a0b0c0d0e0f",
            "AES",
            "CBC",
            "PKCS5Padding",
            "AESCMAC",
            256,
            "",
            "");
    try {
      String[] encrypt = cm.encrypt();
      // key
      assertNotEquals(null, encrypt[0]);
      // encrypted message
      assertNotEquals(null, encrypt[1]);
      // iv
      assertNotEquals(null, encrypt[2]);
      // hash
      assertNotEquals(null, encrypt[3]);
      // macKey
      assertNotEquals(null, encrypt[4]);
      // signature
      assertNotEquals(null, encrypt[5]);
      // signaturekey
      assertNotEquals(null, encrypt[6]);
      // salt
      assertEquals(null, encrypt[7]);
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }

  }

  @Test
  void encryptAesCbcWithoutPasswordWithHMac() {
    CryptoManager cm = new CryptoManager(
            "000102030405060708090a0b0c0d0e0f",
            "AES",
            "CBC",
            "PKCS5Padding",
            "HmacSHA256",
            256,
            "",
            "");
    try {
      String[] encrypt = cm.encrypt();
      // key
      assertNotEquals(null, encrypt[0]);
      // encrypted message
      assertNotEquals(null, encrypt[1]);
      // iv
      assertNotEquals(null, encrypt[2]);
      // hash
      assertNotEquals(null, encrypt[3]);
      // macKey
      assertNotEquals(null, encrypt[4]);
      // signature
      assertNotEquals(null, encrypt[5]);
      // signaturekey
      assertNotEquals(null, encrypt[6]);
      // salt
      assertEquals(null, encrypt[7]);
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }

  }

  @Test
  void encryptAesEcbWithoutPassword() {
    String[] keys = {"", ""};
    CryptoManager cm = new CryptoManager(
            "000102030405060708090a0b0c0d0e0f",
            "AES",
            "ECB",
            "PKCS5Padding",
            "SHA-256",
            256,
            "",
            "");
    try {
      String[] encrypt = cm.encrypt();

      // key
      assertNotEquals(null, encrypt[0]);
      // encrypted message
      assertNotEquals(null, encrypt[1]);
      // iv
      assertEquals(null, encrypt[2]);
      // hash
      assertNotEquals(null, encrypt[3]);
      // macKey
      assertEquals(null, encrypt[4]);
      // signature
      assertNotEquals(null, encrypt[5]);
      // signaturekey
      assertNotEquals(null, encrypt[6]);
      // salt
      assertEquals(null, encrypt[7]);
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }
  }

  @Test
  void encryptAesGcmWithoutPassword() {
    String[] keys = {"", ""};
    CryptoManager cm = new CryptoManager(
            "000102030405060708090a0b0c0d0e0f",
            "AES",
            "GCM",
            "NoPadding",
            "SHA-256",
            256,
            "",
            "");
    try {
      String[] encrypt = cm.encrypt();
      // key
      assertNotEquals(null, encrypt[0]);
      // encrypted message
      assertNotEquals(null, encrypt[1]);
      // iv
      assertNotEquals(null, encrypt[2]);
      // hash
      assertNotEquals(null, encrypt[3]);
      // macKey
      assertEquals(null, encrypt[4]);
      // signature
      assertNotEquals(null, encrypt[5]);
      // signaturekey
      assertNotEquals(null, encrypt[6]);
      // salt
      assertEquals(null, encrypt[7]);
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }

  }

  @Test
  void decryptAesCbcWithoutPassword() {
    String expectedText = "000102030405060708090a0b0c0d0e0f";

    String[] keys = {"00a9d165b22d74ece214db56b93d6503427fb7e4ab1ab8f9aecc7f05a002f554", "437689221409c39588c3deb02623b478", "", "308203473082023906072a8648ce3804013082022c028201010092cab726d8d8c5b2d87653c8edd5fc7a61857b062e979f4ff26075c0ef62ab581378b0376f9867679d3cdad4251c6994455e32e166bd706c9aa471803dba5af7b96a907d7020a38da05fb1b0ffe0987c64e7b65ea3ec24d40b2b857ea502d92f8f9b92fbd4fd3a92ff270343128b86b4e8b2623ed2ba4ef1d9dfd29a9e8f131a989de7ebf1d1f6da77db151ffbffe74d64ae16e26118b70ed86b90685bb78fe31fe0e1858da4641580ee3c19d07284d51434f5269e3a6925511dd692e590de0eff5b062a0552969a62d87f6eb8b2eeceb9b496a9b688d0ab76605391dc5598d166ca05d2d1baa7e4837cb3af51fa9a87414996be8cd6997aba43e6c80f7a2b57022100b1df70eafe0fa6b275981cf22096736a2c58c2a17e0db5125586a2eafa23f0f50282010008416e3c3945d774b0eb978e5ee0471f1d77f35d79d06ec8a6780947193109a096cb59c5939d0ef88ea3f59b43ade02d3cb4e0622485ddce5bc6b30601fb1fc66d3a8a472f03eeb91304acfbbae6fd1aa0caae9286af9acbce4daf5e521b5c07d876643efc906ee08e36a7b0723bdc810a3f337e64558113ca19d3123cd9d360eb501274e5c497ff7390f855e5b673649d1362e8b9b02416b895c8886fc49b91c9870e682348c04423db3436246b057c05b31590896f0eafc1b5aa42ec1c6fc527d631b0e918a36c935544393a94dae9d58ee4f02a9ed8187f24f51096783d304bd862e2de435c47616d57fea327f2ba1718cf6f5168c5a8522f6ae530c81b7d038201060002820101008508d7fafb5ecd700e25ecf24a2068367441b6926c9d19089739fa157d7342910d70664cc22f2b08a2ac8cb76cbb2a186a4eb00c93155acc6dd4f3c1a6a90c9a96b1a53b7d517be0c3a2dcba88dd2b78892b03ff32c39512438a13afca4fcddf49d5f124e6ef78a9bc85112fccdf4e64caf4823150339c16e53bb77aca7e816297a23c7904a28b71d9e9c7716b70a045c172dbd834c9f703d093b4b32521298f735b5bb1af8d78ddb13dbd6bb0dc8bbc8c157475e7f569a158e41b34075fe6a9cce8528f53918cdc61fc2952e7c7ea7561325173a7ec277422b291816cd792802b4dd56e1841a06eb0dc8d7e3e623a3e59d80ca178cf3660c010587a1d7d2b99"};
    CryptoManager cm = new CryptoManager(
            "8e7ae698da0618f5d2fb1f5a9626f4ff6f9057cc16dc0f6776b819f464b7e78b3c89f486acb8eee5276fdedd8dc3f6be",
            "AES",
            "CBC",
            "PKCS5Padding",
            "SHA-256",
            "3306d450465ac3f324510094ad50bf04d1e0a4ad69e0054f01c4de6594cc875d",
            "30440220395266b0e9b2da63a6b3cbbd1d26a612f31da12c3df91581f756f41bb86fb10f022067d89e7ca5a41adec23fdfbf4b26178c04eca70b03d19669141bbfaf5da451ce",
            keys,
            "",
            "",
            "");
    try {
      assertEquals(expectedText, cm.decrypt());
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }
  }

  @Test
  void decryptAesCbcWithPasswordSCrypt() {
    String expectedText = "000102030405060708090a0b0c0d0e0f";

    String[] keys = {"", "a17c2d55e56bf5ca21e6247fe064a16e", "", "308203463082023906072a8648ce3804013082022c02820101008407ee7744e948e5bc97db37633ed1ba39d87cacec4ed5e324ba2c2792985e57e89fb3c97f8f11a3856b7a4822d9e906477bc1d4f8a3cfa758a038a14cf9ff3eab726e1ccffed7bd71ce8740f930889582a1452742a54b18518dce267b7dc7962925ba4ebb69ba093067c2d6710ce9987b61c5feab32f2cb00ac627f3bc68f96197163cbf27eee11a69e5b565b89b29e8a757a9227335d621ef4b31d0a1f1af15252a3992e4c06a5c652e8f2fb6d20095a69d52e58dbe3a8a115b38bcfce795f0237ac29c0e9a7a60d76115d46233a5f20d71cb38a19a1c430f68779e202c2f1a2100a9f3f637cf2afa5d6f1a480f280b9d3fcf102ea7f5243366169a64dd461022100b72f4c15b301c51367abc6b622aa760551b0f737590bfb64eb9c21b0198b4c39028201007a380d8e35ead47695e33d50e0d28b8a82069364124be3bfdbd13dae905ff1f89e62b5df2605a75249b02e90b49383ab4c65936cffd9c99cace11b2019fa37d65500e3be73e24a55b7467249f9d8715fe3f26402f3c9fea95db4240b2d958fcbb0b91a018a3ccb6aeb50d045708439321af19baa5805866df5c99c97b74ff2c5fb4a5d5c4021b80798d564b89db1bc19b3e96d7e6d1d04b4d745bf6c2f0393e6a68eddd74b60f29bb639c9a5fbef68d505bc92c7c4ae704eefd768eef326f5d7480d6bf84353baaae7e4325dec3028396644c8d9dbfb29e66448ba4f9ad0b4451a896505727f4f9bc74c8427c4af45bbeaf2802469f2b4f89e16752fb11cf11c0382010500028201001f10f7946c1e94d91564c978b28edddda4fac5df82447de1803237d7eb46cec5117cc622c3c8bf4c40828b1950221e4c07e8664b4e6b81af5dc444c0241132cfa135c5af4828d6a851d11602dc1767326de688fcd8bc156fbfcefe7531476969b568aa9758f74f00a102f068229e407d62771dbdd07aa1e7004d9e1de1c2fdffe8c0eccdefbf851a1f2fea9513785d567a72c5c4858a8b0e4a01bc6bdf94f656d13cfc6549f73ffb4ed0d8b1be6d6e531f80effa191162bcef8084cd027056810fe9c316a48ea89daf2029e3af9ffd11eb77da34aee0eeac2450436fdb746745109f78d48585e9bb18d73b9016e894dae827584d351c898afeeca1e23583f4ce"};
    CryptoManager cm = new CryptoManager(
            "6f4021bb3828dc9d60bf471c64c019a8f568a17f0f1de27f610007163d8bd9a34e75651ae949315c56c67beed9e38d6c",
            "AES",
            "CBC",
            "PKCS5Padding",
            "SHA-256",
            "c58b702070eee5aff69d986b825c3978eeba17d71a08cf72d8e0712eabc38c3e",
            "30460221009a1f0ab9d199496d00e318671c4ed48e8d65ae1b43a5cf117eba0a5c2ffb9ea202210082270b56eed09b531fb62b4c496c52b04b03de64e1a23074a88c8f026dea464b",
            keys,
            "test123",
            "6231383236373234376636636236663638363862366137636263373765396561",
            "SCrypt");
    try {
      assertEquals(expectedText, cm.decrypt());
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }
  }

  @Test
  void decryptAesCbcWithPasswordPBE() {
    String expectedText = "000102030405060708090a0b0c0d0e0f";

    String[] keys = {"", "34534ad0d5de6f01c0eee7421b507039", "", "308203463082023906072a8648ce3804013082022c0282010100847a4f4a3699e6b202c500e168f7e7858b580511da10a156f96bd58377341fe0aaa3e588d776c5efef7754d5241e6571fc8edef84742a5a154dcfbba8ff8cfa7a2a8a76c20659408eeb686aacc7bfe3cc0dc96716c5e1f121538ecc2c35a2b86ad80976d9d487de5fe01c2199a2ea37f6cd9ebe405fc574f0afc6bba70ef33fd7e7424fb886f5af169a76a75c32c575cfcf7081efc5c4f6c7f8523b34a35f1fe6db5bb2b28007421c529c58bb9d96d49c9189aa75d616c92a9094f75b862ca106a383d13cfaf3479fe520c613354c5a015414eba588170fb2ca99d3780c8980828399d8b006f8c2de43bf29cc8c49375fff3cee3127f15771bc39b63f9d601bf022100eb9685a5d41fbb9c68707b19fff2ab36a6ff96f88e0c95176f13f744ca5d0e9d028201006cc659ac774aa243b970faef72c8418fda720a57ae381ca12d63b220cd9ad564025a8718d379f60efa15a20d6bf4ba6f6b871144e6f8381855ba0811e5c1f5a8a2946f75d33757acb202ff6bca5e16a0996945e8c0860b1b9a8748c58414dda928b62e9a652e7e3d3979b168e9eb7b5c6714896690938c98bb1590a7a3b5bfaf863c5d9903e9fd1224a77a8da4a8fb34be37525e1e1b9fb5b230a8b561d25278acf9bfcb4387ed826104f042d8ac176401e65ec5feceaa37dbbc2726fd4fcbddfcdf41c69a4f80c8a691e964f93d04adc1f829e2a7a6f7dde2e62abf19b89be455fac7b887d459f369932d328376ebbf0ab63e5018688b8855ca15ede956af0b0382010500028201005c463e50b53aa7dc305a1ef8b1cae24761552cc098f3daa5f0d785570b2d6ffd2feedf5709a29d9128404cc4f3b40bbeb8b4c77622921e8fd1bdb08cfe307fcf30ee599d08dd387391d8c2592b2713c46344d3882a62de8bb3bab65a8e3fe6f148934ce196611821a060f6ab4c4c441227d1f50e5b1f260f6cf560de063a8c80db5a2a8c43aa12fcb26f2948e0989c096146459207bcd20ec50e87fd127865ac431618c654016e6d1e049bcf65487cb44d40e4e9333536b4d57315aabb0f3c07b57f91a24d66c1300660e6c540a3af0d9c1e8637fe5ca1d5277b54a4cd05a7820452ea37f7179543ae4944aa9e73740ff9fa628968c57252f463b5072ed8cee8"};
    CryptoManager cm = new CryptoManager(
            "82988a8b48f02822687bd2bcc66698854dad0876dcba6130f559ef07dc950618d690d458c44fcce55271fada06350a10",
            "AES",
            "CBC",
            "PKCS5Padding",
            "SHA-256",
            "09df64435e5a72fe8406c08ea1158206a9491b088232367534ccafd2565f5107",
            "30460221008ac7107d320e6c51102464de9155f620bf8e047e7fff5ac32906384d4fd1c8ee022100d00f5c8886715030cdb701488accbe346871572363e694b076c86369bb92a9e3",
            keys,
            "test123",
            "3966626233343765303661393963383665653436326130386239356330663531",
            "PBEWithSHA256And128BitAES-CBC-BC");
    try {
      assertEquals(expectedText, cm.decrypt());
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }
  }

  @Test
  void decryptAesCbcWithoutPasswordWithAESCMAC() {
    String expectedText = "000102030405060708090a0b0c0d0e0f";

    String[] keys = {"896034f5635c062b342268a98bbf143446564ff085cd9a2b710e68be5d4ff54b", "16452b510412fbfb46c8cd48611a7ec2", "805e9215553432a3c9f7bf528dfa32be08b740b5034fe624e7169cfbd3396b77", "308203473082023906072a8648ce3804013082022c0282010100c32040232c542a6e2719a3a27877eb15bf2f791645a5e4a658d2ec9871f7b05fee0df48f37181f369c521be3f3448de95ef6a1c3fe73f86875df3d1a67623f16794ea44950a063f3771648d2274b60a4a770be8306e2d8bc43b3f062389dd6ecc36b58218a85eb2be51635490811a05e9d2c576e3f7a59a3bd1d9d9e053c075071c69c461cd0c595243f6e252785df8a84bdeea89540e2ab5047912926f0aebd49f05b2ae417a1353dc35bcf726054bb69838ead3386610514e912c7d0c3c7a7ea1f624f06d371f9a4b9ea218a3acc4ee4686380bdb47cf5673350e90b3d4204eaaa78650a5bc80a16f1c09c897c8dacafee4ca779e56efdfbb001a1d8f1dda9022100faf94bc34053223db8bd5a626b5dc52694ad7ad70fd0efe8eb008766652a339f0282010052b30ca86066e4956c55005a2d1a2a17e74529079241133ba23afe56cce8a19d983a337b69b8042d4d749cae41dbcddfd535f8a437dba119c0ffabe2844af1379a73d776de8aeca044f9a9b5243f63053d67b03ae8418505dd47843d12aa6a9072af8c855a8d065d2b762733db70dd38df2e56803d0d8e7e05c3115b9c5475bf22e4f37773176ee54e7ec62e79b6e98f63b7a452a438393663fbcc48929306a3b0055728769f6972d39f69ba2817de4d9747164da913a3273e75b07a7b8b65116d28681bb7dbd1a0e8bd5c5c62ee650970a2b91fc4ee5c95157a199fe6a8c323ab403b621f07964de9d437eda207679eb3f848ed40e967f564e17bb517d2de0203820106000282010100a04def5ac56066de8f28b5cc2b5d14ecb0f1da17c7435a8086ff2fc62547c2774034fb1f428fb9d0dba826a3763186e1fc26155949afa129fc651515a261ebe1dc126b5bb411d13c033261602072ae50ca71118b7c9e7671bbb0f99e68a4dcace41a7d7183022742bcf36465c21a125e9ea4fb4d01b7c0b2c7c40e12028ab54063c607531e4bd9d31594bb769edd51a298ad84de226459a5f42b23fb06e01d0151d16018121f868dc4ff8b339eb3043893f1365864881254801f50a72d2914779152b43b3cbf8bbcfe80d70a97c4c7292df032fa94ae0aaa07a70235089d09dcb08c70d6b79bea7fd33a15d4b3c6da5a40feedda3789324d6759f22599e3a562"};
    CryptoManager cm = new CryptoManager(
            "23ab8e565bbb25062c2172d1372ee6a6ab95a8fe4777f04e24b74f067d06701f16ed824d9157da606dd55c82c6a58d15",
            "AES",
            "CBC",
            "PKCS5Padding",
            "AESCMAC",
            "d21a8261321f901c8e3d9ab8eecb6b11",
            "304502204eb8c73a35a4a8caf70d45fe4f2acde191e1a2857a5048751c77b5a8ce3510c7022100b5cd380ccaca62f533cc2f6bf956f76869cf9956671c6ef2100b115e06d68685",
            keys,
            "",
            "",
            "");
    try {
      assertEquals(expectedText, cm.decrypt());
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }
  }

  @Test
  void decryptAesCbcWithoutPasswordWithHMAC() {
    String expectedText = "000102030405060708090a0b0c0d0e0f";

    String[] keys = {"4c88a0241b393d36f507bb4c5ada5b07aae2efd78037682e91e8caa5bb4bd018", "800af05191ea840394f044af6adf8980", "5b0674cab137efb92c8f41da4cd161a8b0f84a862895885ae477b77d4856f91a", "308203463082023906072a8648ce3804013082022c0282010100a9c78f95b1afcf80119a73457b05c39d09102ff0c7914c7dea659c5cda462fa90b8cf571997d313a1a36b2c18f122289ae90b5533760407a219de3e7b459781ade110b369130353e123d01fa1b022d6252d8269f2ff63c363c799cdd2c657a2b70f14cb9a747356d6eb9232f83b50153e39c9eedfd9653123bddc8b8a53971d8f7baaf02017bd60cafb0834ca638d21b27cac6d41604dff9f8aa3105e105779afabbd7d94d1dab9c7c0b7c9cdf60488020ffaaea3019a58489159013faf11697abc7966534f484c44c83cc9653f4f1fb3e425867a8d05f48584082792267c0b32c7555e9433e70314733203cdd5b04034eda25f54f40928c2eaeb064e1236fd3022100c7f4ac406bb1515c65aae093e486113bbc1ce526e2e70d113a4be47ef26bbd9b028201002d915e4f4635da6e9f0ebb4068ca14a79ce9502e0658227d812e618b9a8acf18eef9b973308eb678a14c06ffe05f01ffa3172531d7b2fb411dcb37c36f79561f4a409b44cfe5c17d032d4d8b44f2a00c6a577eaf6cad2232aee2d5fc26c70d5086a169a29a4ee4d5244ef2d1b58dc0a93e264484992aba87e0a82979fdada42f1a60188d71d071641200337bd05104aeaeea5ef9c89048d1588f15405dac8dfdd435c7a6b157b3368b988017bf8312359fc50545b1bd24a5548c5b9d5d0b0675bcb3b4694f02a7aafbc8bb2e14d8e9d53e98a45da502c34ba18f987fcc48e9fc289aa93f7c3a673f032df84b1e31f3dcf61e29b752b687b92bfa1c45479c1a09038201050002820100243a44f7414cf7424882a41c53371ed6d87743ced620bb519e30f49812b731ebca0019377fb5f79982ffd1ab7b9739c7f6e21d804a629b361db6e2241a0feca97b748aeb0d0ab73b84f217b962b4d3f7ee9122c39ae6231dffa1161fe9c774c819e55d29f9ca22c928ee03aa708623f13b3d17c6fb6373141cc4bba88ff6b5170fc5884f4cb8a0eafee343b9977aa6ca4c401faecc03614bf9fc6543940e92d8ce9df32939ed26483ab3ea3804f8a42991931d425b95eaaccecf19591d7bae8184b17283757068c0e1de1a0d51732aa6ceb68e2e2d8b2fec67aac371b9f91aed4afd17ab552108944d3ebc32d4c1b9eba6b103ae1ffb803d8f3085b50318eb07"};
    CryptoManager cm = new CryptoManager(
            "960dbe1d879ba001d5686ae8a6b89ba8c388f0330597c6bd71e0adf6303deb75c82303f3ad60789472e60f94c69a3d09",
            "AES",
            "CBC",
            "PKCS5Padding",
            "HmacSHA256",
            "65b584b066b52aede738be900fd395ab56e9b19e0015cab4256ef753defc16a1",
            "30440220547b01097aa85baf61dbedb88e96d974062214f7781b1cc5778c9230bccb0c460220296e4e26f8ecab7e2b5b7a2225fd3480242369fcd8705a890a333e6a43861c1b",
            keys,
            "",
            "",
            "");
    try {
      assertEquals(expectedText, cm.decrypt());
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }
  }

  @Test
  void decryptAesEcbWithoutPassword() {
    String expectedText = "000102030405060708090a0b0c0d0e0f";

    String[] keys = {"87a5ece9c41682ba13c77832e7859a2e7b30d573bc1e0a99de9371f42770999d", "", "", "308203463082023906072a8648ce3804013082022c0282010100da88746a172b2adf4bffdfd0e135fcfd2b7852dd3f71296221605394e93c343bd20822682e7cd312f6d306e0e7dbf5883551ca7eaa7125b55fa6bf85e24544a47aa4472c8f503b8bf24a58d46940482bf1f8983b02b502d270a8af9a011972703a659321b91ff69554c757f92aed152bd4ef51419809d5b78f110d2440b9635a443eaa5570fa6720cea69688fd612c9863d3c57d6a87952e15711d61e927e272fea78c85fda54d6306d743e4555c836f73f46ca41acf6618e67359d56545ea2b6336f721bbec0c4f0232ca326dedc476a2d28d7537453188d4ec8362717644ca3db5a5963cfcb88338ed92acbbfc7db04771d7fd443ac3338bfd67ce357beadf0221008e2d2e0d20de67db4cc4efd1bcfb3d956fa8c7a8fda30451eba8ab7dd6cf86ed02820100306b1379bd264e58f1e63f7b7a0defec7c2a5e57c843a56cfcbafb17adff2b537438c8aedeede4828b717457d9132fba903f50b0e25dcb3a26a35bdf9323b2085a56d41cf189aae71e0bb0d891f2bbbf743b248cfc4569cf40458f6ef7fa7f0e2d367d6476167e58c1eacdd8a12847b32016eafb3f5b0253fa134c84b1566616f4b51cbd33dc03c97ba62f8f8c381300dd4a0cb97e04337d0b79b4044f8062181f28edbd1737e2dd322994b1e6e92336a3c1c0f2b998b86a28dc30fc3dc47320fe6bfaffec8ec7dd1cd984463791278a277ecb86cebfbb8bfb7a1370d77aa204d45ca32c8928f0c827223beb6d17d2439ee6298d418dc2558ec234c9ee1026f4038201050002820100522cf52a38473687f047d766e2b6fc81c3b0a4ea706a480949abb3df77104ff10ab08076d890242883ebe37366a9f80563162029d70c2ef53b86db3b8b0c15f41f454b2926f1458537336262007eb61e73cd32fec0aa5cc3b139f6f76bec4dd49143b4181568e1857533505f183a0bd1ef1769559a05c3107e0f57b4244ab77bc761bd76e61c255439fd2e7272552ba940611ba9da8ecefd72a18d39f7c83f002eb2c79781a537f21d73930109bfa072486c7d881b9e58ef22626c9c2213ceb5b297c1a75e2baed31f700944d9fde5a7e2cd434053f5d5c34aba78fa7e7c3670d37ceb424cb1285101ac61c1d5efe13676e7e561f329a6c3e416b1e39da8b246"};
    CryptoManager cm = new CryptoManager(
            "f8b38a0cb4d637de92522063beda116ff9787362f9b1515b8f28d5aa0d9229073b8d6607cd4979a366a219cc2a623435",
            "AES",
            "ECB",
            "PKCS5Padding",
            "SHA-256",
            "a8aa9c504fc5af988603c68e7b965b6b3c6474552ababb2944d3ea026cbe1dd7",
            "30450221008bdcc9d7f9d7e73c5dbecccd216603f9c8df0a9d9d491c686ea2b339b92bca9f0220775674e7d609cf67a7e01ec44c668dbc957d60f389b20cba94dcebc4ea0a241d",
            keys,
            "",
            "",
            "");
    try {
      assertEquals(expectedText, cm.decrypt());
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }
  }

  @Test
  void verifyMacFail() {
    String[] keys = {"896034f5635c062b342268a98bbf143446564ff085cd9a2b710e68be5d4ff54b", "16452b510412fbfb46c8cd48611a7ec2", "805e9215553432a3c9f7bf528dfa32be08b740b5034fe624e7169cfbd3396b77", "308203473082023906072a8648ce3804013082022c0282010100c32040232c542a6e2719a3a27877eb15bf2f791645a5e4a658d2ec9871f7b05fee0df48f37181f369c521be3f3448de95ef6a1c3fe73f86875df3d1a67623f16794ea44950a063f3771648d2274b60a4a770be8306e2d8bc43b3f062389dd6ecc36b58218a85eb2be51635490811a05e9d2c576e3f7a59a3bd1d9d9e053c075071c69c461cd0c595243f6e252785df8a84bdeea89540e2ab5047912926f0aebd49f05b2ae417a1353dc35bcf726054bb69838ead3386610514e912c7d0c3c7a7ea1f624f06d371f9a4b9ea218a3acc4ee4686380bdb47cf5673350e90b3d4204eaaa78650a5bc80a16f1c09c897c8dacafee4ca779e56efdfbb001a1d8f1dda9022100faf94bc34053223db8bd5a626b5dc52694ad7ad70fd0efe8eb008766652a339f0282010052b30ca86066e4956c55005a2d1a2a17e74529079241133ba23afe56cce8a19d983a337b69b8042d4d749cae41dbcddfd535f8a437dba119c0ffabe2844af1379a73d776de8aeca044f9a9b5243f63053d67b03ae8418505dd47843d12aa6a9072af8c855a8d065d2b762733db70dd38df2e56803d0d8e7e05c3115b9c5475bf22e4f37773176ee54e7ec62e79b6e98f63b7a452a438393663fbcc48929306a3b0055728769f6972d39f69ba2817de4d9747164da913a3273e75b07a7b8b65116d28681bb7dbd1a0e8bd5c5c62ee650970a2b91fc4ee5c95157a199fe6a8c323ab403b621f07964de9d437eda207679eb3f848ed40e967f564e17bb517d2de0203820106000282010100a04def5ac56066de8f28b5cc2b5d14ecb0f1da17c7435a8086ff2fc62547c2774034fb1f428fb9d0dba826a3763186e1fc26155949afa129fc651515a261ebe1dc126b5bb411d13c033261602072ae50ca71118b7c9e7671bbb0f99e68a4dcace41a7d7183022742bcf36465c21a125e9ea4fb4d01b7c0b2c7c40e12028ab54063c607531e4bd9d31594bb769edd51a298ad84de226459a5f42b23fb06e01d0151d16018121f868dc4ff8b339eb3043893f1365864881254801f50a72d2914779152b43b3cbf8bbcfe80d70a97c4c7292df032fa94ae0aaa07a70235089d09dcb08c70d6b79bea7fd33a15d4b3c6da5a40feedda3789324d6759f22599e3a562"};
    CryptoManager cm = new CryptoManager(
            "23ab8e565bbb25062c2172d1372ee6a6ab95a8fe4777f04e24b74f067d06701f16ed824d9157da606dd55c82c6a58d15",
            "AES",
            "CBC",
            "PKCS5Padding",
            "AESCMAC",
            "d21a8262321f901c8e3d9ab8eecb6b11",
            "304502204eb8c73a35a4a8caf70d45fe4f2acde191e1a2857a5048751c77b5a8ce3510c7022100b5cd380ccaca62f533cc2f6bf956f76869cf9956671c6ef2100b115e06d68685",
            keys,
            "",
            "",
            "");
    try {
      cm.decrypt();
      fail();
    } catch (Exception e) {
      assertEquals("Mac not equal", e.getMessage());
    }
  }

  @Test
  void verifyHashFail() {
    String[] keys = {"87a5ece9c41682ba13c77832e7859a2e7b30d573bc1e0a99de9371f42770999d", "", "", "308203463082023906072a8648ce3804013082022c0282010100da88746a172b2adf4bffdfd0e135fcfd2b7852dd3f71296221605394e93c343bd20822682e7cd312f6d306e0e7dbf5883551ca7eaa7125b55fa6bf85e24544a47aa4472c8f503b8bf24a58d46940482bf1f8983b02b502d270a8af9a011972703a659321b91ff69554c757f92aed152bd4ef51419809d5b78f110d2440b9635a443eaa5570fa6720cea69688fd612c9863d3c57d6a87952e15711d61e927e272fea78c85fda54d6306d743e4555c836f73f46ca41acf6618e67359d56545ea2b6336f721bbec0c4f0232ca326dedc476a2d28d7537453188d4ec8362717644ca3db5a5963cfcb88338ed92acbbfc7db04771d7fd443ac3338bfd67ce357beadf0221008e2d2e0d20de67db4cc4efd1bcfb3d956fa8c7a8fda30451eba8ab7dd6cf86ed02820100306b1379bd264e58f1e63f7b7a0defec7c2a5e57c843a56cfcbafb17adff2b537438c8aedeede4828b717457d9132fba903f50b0e25dcb3a26a35bdf9323b2085a56d41cf189aae71e0bb0d891f2bbbf743b248cfc4569cf40458f6ef7fa7f0e2d367d6476167e58c1eacdd8a12847b32016eafb3f5b0253fa134c84b1566616f4b51cbd33dc03c97ba62f8f8c381300dd4a0cb97e04337d0b79b4044f8062181f28edbd1737e2dd322994b1e6e92336a3c1c0f2b998b86a28dc30fc3dc47320fe6bfaffec8ec7dd1cd984463791278a277ecb86cebfbb8bfb7a1370d77aa204d45ca32c8928f0c827223beb6d17d2439ee6298d418dc2558ec234c9ee1026f4038201050002820100522cf52a38473687f047d766e2b6fc81c3b0a4ea706a480949abb3df77104ff10ab08076d890242883ebe37366a9f80563162029d70c2ef53b86db3b8b0c15f41f454b2926f1458537336262007eb61e73cd32fec0aa5cc3b139f6f76bec4dd49143b4181568e1857533505f183a0bd1ef1769559a05c3107e0f57b4244ab77bc761bd76e61c255439fd2e7272552ba940611ba9da8ecefd72a18d39f7c83f002eb2c79781a537f21d73930109bfa072486c7d881b9e58ef22626c9c2213ceb5b297c1a75e2baed31f700944d9fde5a7e2cd434053f5d5c34aba78fa7e7c3670d37ceb424cb1285101ac61c1d5efe13676e7e561f329a6c3e416b1e39da8b246"};
    CryptoManager cm = new CryptoManager(
            "f8b38a0cb4d637de92522063beda116ff9787362f9b1515b8f28d5aa0d9229073b8d6607cd4979a366a219cc2a623435",
            "AES",
            "ECB",
            "PKCS5Padding",
            "SHA-256",
            "a8aa9c504f5af988603c68e7b965b6b3c6474552ababb2944d3ea026cbe1dd7",
            "30450221008bdcc9d7f9d7e73c5dbecccd216603f9c8df0a9d9d491c686ea2b339b92bca9f0220775674e7d609cf67a7e01ec44c668dbc957d60f389b20cba94dcebc4ea0a241d",
            keys,
            "",
            "",
            "");
    try {
      cm.decrypt();
    } catch (Exception e) {
      assertEquals("Hash not equal", e.getMessage());
    }
  }

  @Test
  void verifyDigitalSignatureFail() {
    String[] keys = {"87a5ece9c41682ba13c77832e7859a2e7b30d573bc1e0a99de9371f42770999d", "", "", "308203463082023906072a8648ce3804013082022c0282010100da88746a172b2adf4bffdfd0e135fcfd2b7852dd3f71296221605394e93c343bd20822682e7cd312f6d306e0e7dbf5883551ca7eaa7125b55fa6bf85e24544a47aa4472c8f503b8bf24a58d46940482bf1f8983b02b502d270a8af9a011972703a659321b91ff69554c757f92aed152bd4ef51419809d5b78f110d2440b9635a443eaa5570fa6720cea69688fd612c9863d3c57d6a87952e15711d61e927e272fea78c85fda54d6306d743e4555c836f73f46ca41acf6618e67359d56545ea2b6336f721bbec0c4f0232ca326dedc476a2d28d7537453188d4ec8362717644ca3db5a5963cfcb88338ed92acbbfc7db04771d7fd443ac3338bfd67ce357beadf0221008e2d2e0d20de67db4cc4efd1bcfb3d956fa8c7a8fda30451eba8ab7dd6cf86ed02820100306b1379bd264e58f1e63f7b7a0defec7c2a5e57c843a56cfcbafb17adff2b537438c8aedeede4828b717457d9132fba903f50b0e25dcb3a26a35bdf9323b2085a56d41cf189aae71e0bb0d891f2bbbf743b248cfc4569cf40458f6ef7fa7f0e2d367d6476167e58c1eacdd8a12847b32016eafb3f5b0253fa134c84b1566616f4b51cbd33dc03c97ba62f8f8c381300dd4a0cb97e04337d0b79b4044f8062181f28edbd1737e2dd322994b1e6e92336a3c1c0f2b998b86a28dc30fc3dc47320fe6bfaffec8ec7dd1cd984463791278a277ecb86cebfbb8bfb7a1370d77aa204d45ca32c8928f0c827223beb6d17d2439ee6298d418dc2558ec234c9ee1026f4038201050002820100522cf52a38473687f047d766e2b6fc81c3b0a4ea706a480949abb3df77104ff10ab08076d890242883ebe37366a9f80563162029d70c2ef53b86db3b8b0c15f41f454b2926f1458537336262007eb61e73cd32fec0aa5cc3b139f6f76bec4dd49143b4181568e1857533505f183a0bd1ef1769559a05c3107e0f57b4244ab77bc761bd76e61c255439fd2e7272552ba940611ba9da8ecefd72a18d39f7c83f002eb2c79781a537f21d73930109bfa072486c7d881b9e58ef22626c9c2213ceb5b297c1a75e2baed31f700944d9fde5a7e2cd434053f5d5c34aba78fa7e7c3670d37ceb424cb1285101ac61c1d5efe13676e7e561f329a6c3e416b1e39da8b246"};
    CryptoManager cm = new CryptoManager(
            "f8b38a0cb4d637de92522063beda116ff9787362f9b1515b8f28d5aa0d9229073b8d6607cd4979a366a219cc2a623435",
            "AES",
            "ECB",
            "PKCS5Padding",
            "SHA-256",
            "a8aa9c504fc5af988603c68e7b965b6b3c6474552ababb2944d3ea026cbe1dd7",
            "30450223008bdcc9d7f9d7e73c5dbecccd216603f9c8df0a9d9d491c686ea2b339b92bca9f0220775674e7d609cf67a7e01ec44c668dbc957d60f389b20cba94dcebc4ea0a241d",
            keys,
            "",
            "",
            "");
    try {
      cm.decrypt();
    } catch (Exception e) {
      assertEquals("error decoding signature bytes.", e.getMessage());
    }
  }

  @Test
  void decryptAesGcmWithoutPassword() {
    String expectedText = "000102030405060708090a0b0c0d0e0f";

    String[] keys = {"d0b2f750b904b708f89b962bc589ccf4d4e237d61f3418e91933257401bfd21a", "9e6ea5bc17496c608af09241205c709c", "", "308203473082023906072a8648ce3804013082022c0282010100b4815b7146afac9a13cf8a588bcfda3bf16ad248822b2b020a6e4b1ec0077515ea14a571f829ccef7818075ade54bc633e853e104c9502eed5370f2d60676ce943b25fce02536e3a0a6a822679bdfcbabd7c687cb5f2ec34b363668360eb342566d3f1127175e74cd617d0b7a6b89630a098268aafa7ee28b2c492501313a2b0a43a5b7a7b489605e7a8bba068b17ebdd029ba0ab701f9e369c4d86cccd0e44dd960037fd7c43d07bc4c3e4fee540b96c5a471937802dcf8ab5438787fb23688187090d0e6431d886268b0c2a3b84a93a0a88bfcc330f719a271164428cd919c5207c5b7714f78c09e0633a809cb553a42767329024c7e68c563bdc297027bc7022100b84ecc7b26106b19505f5434e33c30aa72af7b8985f89befd9bde77019943bd7028201005b6ab05c5057345b8900446fa9befaf4ca9b3fdedbedc53ee2f64c331f7e6dd1c9fe18b02c1d05d12ca997bce2127523d3d79b77567e9371c32fdae434d2a8b582bd930519bf4af83e8f3e58ef233dd2518622544279a5c3557c7f54b6da93e7a8485615d741dec7933ce067f45ec40c1312062fe3be08c99183b2624a007d55925c8b88b62505934436092040a7bb20d7eb501262b39fe91568bd57e6951d3bd7c2aa67c3e70020991424548b6b684c4194a9eaa2ab05030733b23beecb524d26604bb15437cab439e0ac5e8362e50e43660a2a73066ad386e4cc4156e13c795d95cd733f9f1dc74a55e5121b16ecc91c8eaf5935bc6509d3edc310a5d2b38403820106000282010100a67bafc96a321c69ba9da7230d6561b855f00e3500cabd926fcd64715ce5a6f6ce8b8bb2c00a6a18af094d5f91e2d8c5e14b0da5cbc31e3df0433599ff577a11f68fa881a5dc6758a9552d07722f55b0f69fbaf3629c214cbd8af8793d8cf07ef5f684fea44e8ec9732a758e150174944fa6b9380750ef0d11528868c8dcc4b939f549b4311ecbae9394e5cee2d215934fc94fe02774087391b427ec73358c425254227ef4074454577e4e5b69d102af402a705ac1630da8a06bc482a37c1353c1b98eb182d7544a3abc4f13f9fb25ff2ed1637b468bdd800187e8b97b5e45bb14562362ab26dcc339cd04e80b2f1b3c243535a0a124f30a657f2e4017513038"};
    CryptoManager cm = new CryptoManager(
            "0a74aed70a68b3667dfcd41fa07af2c906e763e5c016791f9dfb8207a9b00b7b4470e531255ecab0a69bce5e64b08b40",
            "AES",
            "GCM",
            "NoPadding",
            "SHA-256",
            "4b074f84d02edaae6a42a2a6a2a52750d3e5da435a908767f455ed00430cf9ef",
            "3044022072ec1a13b36775748314caa55c8949ecf70f42f059fcd3c7bbdb788fa7057554022021630f9045215cbf5f6d09367f27c77450fd2299c5bb48a026b94f0ad17aecb2",
            keys,
            "",
            "",
            "");
    try {
      assertEquals(expectedText, cm.decrypt());
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }
  }
}
